# chd-Rcode

This is an R code implementation of the CHD algorithm, described in:

[Eugenio Cesario, Paschal I. Uchubilo, Andrea Vinci, Xiaotian Zhu:
Multi-density urban hotspots detection in smart cities: A data-driven approach and experiments. Pervasive Mob. Comput. 86: 101687 (2022)](https://www.sciencedirect.com/science/article/abs/pii/S1574119222001018?via%3Dihub) 


