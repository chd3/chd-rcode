library(ggplot2)
#library(factoextra)
library(RANN)
library(zoo)
library(dbscan)

nndis = function(datasemple, knumber) {
  nnresult<-nn2(data=datasemple, k=knumber + 1,searchtype=c("standard", "priority", "radius"),eps=0)
  knndistance <- nnresult$nn.dists
  return(knndistance)
}


#compute the density point
Den = function(KDist , k ){
  #tmp = as.numeric(KDist)
  tmp = k/tmp
  return(tmp)
}



#compute the density variation 
DenVar = function(KDist){## return Denv for each obseervation with the accosiate index of the obs for further analysis
  index = 1:length(KDist)
  hahawII = as.numeric(KDist)
  
  data = as.data.frame(cbind(hahawII,index))
  data = data[order(data$hahawII),]
  t = data$hahawII 
  L = list()
  
  for (i in 1:length(t)-1) {
    L[i] = (t[i+1]-t[i])/t[i]
  }
  
  #Important: it seems that the computation is performed on the
  #distance variation, but 
  #it is clear that the density variation is mathematically equal to 
  # to distance variation
  

  L[length(t)] = 0 #we add the last element whose variation can not be computed
  denvar = unlist(L)
  output = as.data.frame(cbind(denvar,data$index[1:(length(hahawII))]))


  
  colnames(output) = c("denvar","Index")
  return(output)
}



# this function computes the denisty variations,
ComputeDensityVariations = function(kDistances){## return Denv for each obseervation with the accosiate index of the obs for further analysis
  #hahawII = as.numeric(KDist)
  kDists = list()
  for (i in 1:length(kDistances) - 1) {
    kDists[i] = (kDistances[i+1] - kDistances[i])/kDistances[i]
  }
  #Important: it seems that the computation is performed on the
  #distance variation, but
  #it is clear that the density variation is mathematically equal to 
  # to distance variation
  # we add the last element whose variation can not be computed
  kDists[length(kDistances)] = 0 
  #change from list to vector
  kDists = unlist(kDists)
  return(kDists)
}#ComputeDensityVariations functions


# this function computes the denisty variations,
ComputeDensityVariationsTER = function(kDistances){## return Denv for each obseervation with the accosiate index of the obs for further analysis
  #hahawII = as.numeric(KDist)
  kDists = list()
  for (i in 1:length(kDistances) - 1) {
    if( kDistances[i+1]==0){
      kDists[i]=0;
    } else {
      kDists[i] = 2*(kDistances[i+1] - kDistances[i])/(kDistances[i+1]+kDistances[i])
    }
  }
  #Important: it seems that the computation is performed on the
  #distance variation, but 
  #it is clear that the density variation is mathematically equal to 
  # to distance variation
  # we add the last element whose variation can not be computed
  kDists[length(kDistances)] = 0 
  #change from list to vector
  kDists = unlist(kDists)
  return(kDists)
}#ComputeDensityVariations functions


# this function computes the denisty variations, but avoiding to divide for kDistances[i] to get rid of some division by 0.
ComputeDensityVariationsBIS = function(kDistances){## return Denv for each obseervation with the accosiate index of the obs for further analysis
  #hahawII = as.numeric(KDist)
  kDists = list()
  for (i in 1:length(kDistances) - 1) {
    kDists[i] = (kDistances[i+1] - kDistances[i])
  }
  #Important: it seems that the computation is performed on the
  #distance variation, but 
  #it is clear that the density variation is mathematically equal to 
  # to distance variation
  # we add the last element whose variation can not be computed
  kDists[length(kDistances)] = 0 
  #change from list to vector
  kDists = unlist(kDists)
  return(kDists)
}#ComputeDensityVariations functions



#compute density level set splits
ComputeDensityLevelSets = function(densityVariations, thresholdValue){
  curr_DLS_Id = -1 # the dls number, not cluster number!!!
  inputVector = densityVariations
  inputThrd = thresholdValue
  outputVector = numeric(length(inputVector)) #DLSs to be computed
  firstAbove = FALSE 
  for (i in 1:length(inputVector)) {
    if (i == 1) {
      curr_DLS_Id = 1
      outputVector[i] = curr_DLS_Id
      if (inputVector[i] <= inputThrd) {
        firstAbove=FALSE
      }else {
        firstAbove=TRUE
      } 
    }#if
    else if (inputVector[i] <= inputThrd) {
      #same of the previous dls
      outputVector[i] = curr_DLS_Id
      firstAbove = FALSE
    }#else if 
    else if (inputVector[i] > inputThrd) {
      if (firstAbove == FALSE) {
        #new dls
        firstAbove = TRUE
        curr_DLS_Id = curr_DLS_Id + 1
      } #if
      outputVector[i] = curr_DLS_Id
    } #else if
  } #for
  return(outputVector)
}#ComputeDensityLevelSets function

# compute eps_i for each DensityLevelSet i;
# the "data" input variable is the working data in the main
ComputeEpsValues = function(data){
  #we create epsValues structures to store eps values for each dls 
  numDLS = max(data$dls)
  #we create two empty vectors, to store computation results
  dlsIDs = integer(numDLS)
  epsValues = double(numDLS)
  for (i in 1:numDLS){
    kDistances = data[which(data$dls == i),]$kDist
    maxKDistances = max(kDistances)
    medianKDistances = median(kDistances)
    meanKDistances = mean(kDistances)
    epsValue= maxKDistances * sqrt(medianKDistances/meanKDistances)
    dlsIDs[i] = i
    epsValues[i] = epsValue
  }#for
  DLS_EpsValues = data.frame(dlsIDs, epsValues, stringsAsFactors=FALSE)
  return(DLS_EpsValues)
}#ComputeEpsValues

ComputeEpsValues_RollMean = function(data){
  #we create epsValues structures to store eps values for each dls 
  numDLS_RollMean = max(data$dls_RollMean)
  #we create two empty vectors, to store computation results
  dlsIDs_RollMean = integer(numDLS_RollMean)
  epsValues_RollMean = double(numDLS_RollMean)
  for (i in 1:numDLS_RollMean){
    kDistances = data[which(data$dls_RollMean == i),]$kDist
    maxKDistances = max(kDistances)
    medianKDistances = median(kDistances)
    meanKDistances = mean(kDistances)
    epsValue_RollMean= maxKDistances * sqrt(medianKDistances/meanKDistances)
    dlsIDs_RollMean[i] = i
    epsValues_RollMean[i] = epsValue_RollMean
  }#for
  DLS_EpsValues_RollMean = data.frame(dlsIDs_RollMean, epsValues_RollMean, stringsAsFactors=FALSE)
  return(DLS_EpsValues_RollMean)
}#ComputeEpsValues

# compute eps_i for each DensityLevelSet i; 
# taking into account only the maxKdist, without the median and mean contribution
# the "data" input variable is the working data in the main
ComputeEpsValues_RollMeanBIS = function(data){
  #we create epsValues structures to store eps values for each dls 
  numDLS_RollMean = max(data$dls_RollMean)
  #we create two empty vectors, to store computation results
  dlsIDs_RollMean = integer(numDLS_RollMean)
  epsValues_RollMean = double(numDLS_RollMean)
  for (i in 1:numDLS_RollMean){
    kDistances = data[which(data$dls_RollMean == i),]$kDist
    maxKDistances = max(kDistances)
    medianKDistances = median(kDistances)
    meanKDistances = mean(kDistances)
    epsValue_RollMean= maxKDistances ##modificato questa
    dlsIDs_RollMean[i] = i
    epsValues_RollMean[i] = epsValue_RollMean
  }#for
  DLS_EpsValues_RollMean = data.frame(dlsIDs_RollMean, epsValues_RollMean, stringsAsFactors=FALSE)
  return(DLS_EpsValues_RollMean)
}#ComputeEpsValues

#  compute eps_i for each DensityLevelSet i; 
# taking into account only the maxKdist, without the median and mean contribution
# the "data" input variable is the working data in the main
ComputeEpsValues_Andrea = function(data){
  #we create epsValues structures to store eps values for each dls 
  numDLS_RollMean = max(data$dls_Andrea)
  #we create two empty vectors, to store computation results
  dlsIDs_RollMean = integer(numDLS_RollMean)
  epsValues_RollMean = double(numDLS_RollMean)
  for (i in 1:numDLS_RollMean){
    kDistances = data[which(data$dls_Andrea == i),]$kDist
    maxKDistances = max(kDistances)
    medianKDistances = median(kDistances)
    meanKDistances = mean(kDistances)
    epsValue_RollMean= maxKDistances ##modificato questa
    dlsIDs_RollMean[i] = i
    epsValues_RollMean[i] = epsValue_RollMean
  }#for
  DLS_EpsValues_RollMean = data.frame(dlsIDs_RollMean, epsValues_RollMean, stringsAsFactors=FALSE)
  return(DLS_EpsValues_RollMean)
}#ComputeEpsValues


runDBScan_Instances = function(data, densityVarMode, dls_eps_list, minPoints) {
  detectedClusters = integer(nrow(data))
  currInitialInstanceRow = 1
  currentInitialClusterID = 1
  for (i in 1:nrow(dls_eps_list)) {
    currentDLS = dls_eps_list[i,1]
    currentEps = dls_eps_list[i,2]
    if (densityVarMode == "linear") {
      currentDLSdata = data[which(data$dls == currentDLS),2:3]
    }
    else if (densityVarMode == "roll_mean") {
      currentDLSdata = data[which(data$dls_RollMean == currentDLS),2:3]
    }
    else if (densityVarMode == "andrea") {
      currentDLSdata = data[which(data$dls_Andrea == currentDLS),2:3]
    }
    currMinPoints = minPoints
    currentDBSResult = dbscan(currentDLSdata,eps = currentEps, minPts = currMinPoints)
    #store clustering results in the clusters vector
    currNumInstances = nrow(currentDLSdata)
    currentNumClusters = max(currentDBSResult$cluster)
    #update the clusters vector by adding the clusters detected
    #during the current dbscan run (clusterID is updated to deal with different DLS)
    for (j in 1:currNumInstances){
      if (currentDBSResult$cluster[j] == 0) {
        clstID = 0
      }
      else {
        clstID = currentDBSResult$cluster[j] + currentInitialClusterID - 1
      }
      detectedClusters[j + currInitialInstanceRow - 1] = clstID
    }
    #update some variables for the next iteration
    currInitialInstanceRow = currInitialInstanceRow + currNumInstances
    currentInitialClusterID = currentInitialClusterID + currentNumClusters
  }#for
  return(detectedClusters)
}#runDBScan_Instances

runDBScan_Instances_RepOutlier = function(data, densityVarMode, dls_eps_list, minPoints) {
  data2=data
  data2$detectedClusters = rep(-1,nrow(data2))
  currInitialInstanceRow = 1
  currentInitialClusterID = 1
  for (i in 1:nrow(dls_eps_list)) {
    currentDLS = dls_eps_list[i,1]
    currentEps = dls_eps_list[i,2]
    if (densityVarMode == "linear") {
      currentDLSdata = data2[which(data2$dls == currentDLS | data2$detectedClusters == 0),2:3]
    }
    else if (densityVarMode == "roll_mean") {
      currentDLSdata = data2[which(data2$dls_RollMean == currentDLS  | data2$detectedClusters == 0),2:3]
    }
    else if (densityVarMode == "andrea") {
      currentDLSdata = data2[which(data2$dls_Andrea == currentDLS  | data2$detectedClusters == 0),2:3]
    }
    currMinPoints = minPoints
    currentDLSdata$cluster = dbscan(currentDLSdata,eps = currentEps, minPts = currMinPoints)$cluster
    currentDLSdata$id_instance=data2$id_instance[which(data2$dls_Andrea == currentDLS  | data2$detectedClusters == 0)]
    #store clustering results in the clusters vector
    currNumInstances = nrow(currentDLSdata)
    currentNumClusters = max(currentDLSdata$cluster)
    #update the clusters vector by adding the clusters detected
    #during the current dbscan run (clusterID is updated to deal with different DLS)
    for (j in 1:currNumInstances){
      if (currentDLSdata$cluster[j] != 0) {
        currentDLSdata$cluster[j]=currentDLSdata$cluster[j]+currentInitialClusterID - 1
      }
      data2$detectedClusters[data2$id_instance==currentDLSdata$id_instance[j]]=currentDLSdata$cluster[j]
    }
    str(data2)
    str(currentDLSdata)
    
    
    #update some variables for the next iteration
    currentInitialClusterID = currentInitialClusterID + currentNumClusters
  }#for
  return(data2$detectedClusters)
}#runDBScan_Instances


#ggplot(datasample,aes(x=x,y=y))+geom_point()

#    OR

#ggplot(bd)+geom_point(aes(x=x,y=y))



CHD <- function (datasample, kvalue, omegavalue, windowSize = -1) {
  workingData = datasample
  clusters = integer(nrow(datasample))
  #add the columns id_instance to the dataset
  ids <- 1:nrow(workingData)
  workingData <- cbind(id_instance = ids, workingData)
  #cbind() aggiunge come prima colonna, usare $ aggiunge in coda
  #workingData$id_instance = ids
  
  K= kvalue ## number of kdist neighbour to choose
  omega = omegavalue

  # compute Euclidean distances among all points
  # compute the K-Dist distances among all points
  kDistances = nndis(datasample, K)[,K+1]

  # compute the density variation for the K-Dist neighbours
  workingData$kDist = kDistances
  #sort kDistances in ascending order
  workingData = workingData[order(workingData$kDist),]
  
  densityVariations = ComputeDensityVariations(workingData$kDist)
  workingData$denvar = densityVariations
  

  
  #classic mode, no roll mean
  if (windowSize == - 1) {
    thrd = mean(workingData$denvar) + omega * sqrt(var(workingData$denvar))
    densityLevelSets = ComputeDensityLevelSets(workingData$denvar, thrd)
    #we create epsValues structures to store eps values for each dls 
    workingData$dls = densityLevelSets
    # compute eps_i for each DensityLevelSet i
    epsValues = ComputeEpsValues(workingData) 
    #compute clustering by running several dbscan instances 
    densityVariationMode = "linear"
    clusters = runDBScan_Instances(workingData, densityVariationMode, epsValues, kvalue)
    workingData$clusterID = clusters
    #sort workingData by instanceId
    workingData = workingData[order(workingData$id_instance),]
    clusters = workingData$clusterID 
  }#if
  else if (windowSize >= 1) {
    #====== Using the RollMean of DenVar
    rolldenvar = rollmean(workingData$denvar, windowSize)
  
    rolldenvar = c(rolldenvar,rep(0,windowSize-1))
    workingData$rolldenvar = rolldenvar
    thrd_rollmean = mean(workingData$rolldenvar) + omega* sqrt(var(workingData$rolldenvar))
    plot(workingData$rolldenvar, type="l")
    abline(h=thrd_rollmean)
    #plot(rollmax(denvar$denvar,1000),log="y") ##logscale
    densityLevelSetsRollMean = ComputeDensityLevelSets(workingData$rolldenvar, thrd_rollmean)
    workingData$dls_RollMean = densityLevelSetsRollMean
    epsValues_RollMean = ComputeEpsValues_RollMean(workingData)
    #compute clustering by running several dbscan instances 
    densityVariationMode = "roll_mean"
    clusters = runDBScan_Instances(workingData, densityVariationMode, epsValues_RollMean, kvalue)
    workingData$clusterID_RollMean = clusters
    #sort workingData by instanceId
    workingData = workingData[order(workingData$id_instance),]
    clusters = workingData$clusterID_RollMean 
  }#else if
  return(clusters)
}#CHD function 






###############  MAIN ##################




#originalDataset = read.csv("crimedata-nodups-xy.csv")
# originalDataset = read.csv("crimedata-Eugenio-Sample.csv")
# 
# omegavalue = 0.7
# kvalue = 2
# windowSize = 3
# 
# datasample = originalDataset
# test = datasample[,1:2] # same format of datasample,
#                         # only selecting the first two columns
# 
# #plot the dataset and its structure
# plot(test)
# nrow(test)
# ncol(test)
# 
# 
# clusters = CHD(test, kvalue, omegavalue, windowSize)

