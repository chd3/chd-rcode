library(ggplot2)
library(RANN)
library(zoo)
library(dbscan)
library(stats)
library(urca)
library(forecast)

source("chd-Roll_Mean.R")
source("map-utils.R")



computeRollDenvar<-function(serie,windowSize){
  halfSize=windowSize/2
  rolldenvar = rollmean(serie, windowSize,fill=c(serie[1],0,serie[length(serie)]))
  #aggiunto Eug-Andrea
  #rolldenvar = c(rolldenvar,rep(0,windowSize-1))
  return( rolldenvar)
}

computeClusterAvgDensity<-function(kDists, kValue, clusterIDs){
  pointDensity=kValue / (kDists*kDists*3.14)
  clusterAvgDensity=rep(0,max(clusterIDs))
  for( i in 0:(max(clusterIDs)+1)){
    clusterAvgDensity[i+1]=mean(pointDensity[which(clusterIDs==i)])
    print(clusterAvgDensity[i+1])
    if(is.infinite(clusterAvgDensity[i+1])==TRUE){
      clusterAvgDensity[i+1]=0
    }
  }
  return(clusterAvgDensity)
}

pngStart = function(filename="newoutput",number=-1){
  name=""
  
  if(number<0){
    name=paste(filename,".png",sep="")
  }
  else{
    name=paste(filename,format(number,width = 2),".png",sep="")
  }
  
  png(filename = name,
      width = 480, height = 480, units = "px", pointsize = 12,
      bg = "white",  res = NA,
      type = c("cairo", "cairo-png", "Xlib", "quartz"))
}
pngEnd = function() {
  dev.off()
}

pngPlot=FALSE
pngNumber=0
mapPointsSize=0.25
pngBaseName="Chicago-ExOpts4-"
crimesData2012Features<-readRDS(file="./crimesData2012Features.rds")

originalDataset=data.frame(x=crimesData2012Features$x_coordinate,y=crimesData2012Features$y_coordinate)
omegavalue = -0.27 #2.55 #-0.27 #0.001
kvalue = 64#4 #64#
windowSize = 5000
titlePart=paste("(o,k,w):(",omegavalue,",",kvalue,",",windowSize,")",sep="")



datasample = originalDataset
test = datasample[,1:2] # same format of datasample,
# only selecting the first two columns
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(c(0, 1), c(0, 1), ann = F, bty = 'n', type = 'n', xaxt = 'n', yaxt = 'n')
text(x = 0.5, y = 0.5, paste("CHICAGO 2012 Dataset.\n",
                             length(test$x),"points","\n",
                             "CHD parameters:\n",titlePart,"\n"
),cex = 1.6, col = "black")
if(pngPlot)pngEnd()

#plot the dataset and its structure
#plot(test)
#title(main="CHICAGO 2012",sub=paste(length(test$x),"points"))
nrow(test)
ncol(test)
mapTitle=paste("CHICAGO 2012,","#points: ",length(test$x),sep="")
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot_map(crimesData2012Features$longitude,crimesData2012Features$latitude,c=rep(0,length(crimesData2012Features$x_coordinate)),png=FALSE,file="myoutput.png", title=mapTitle,legend=FALSE,pointsSize=mapPointsSize)
if(pngPlot)pngEnd()




# ------------
workingData = datasample
clusters = integer(nrow(datasample))
#add the columns id_instance to the dataset
ids <- 1:nrow(workingData)
workingData <- cbind(id_instance = ids, workingData)

K= kvalue ## number of kdist neighbour to choose
omega = omegavalue

# compute Euclidean distances among all points
# compute the K-Dist distances among all points
kDistances = nndis(datasample, K)[,K+1]

# compute the density variation for the K-Dist neighbours
workingData$kDist = kDistances
#sort kDistances in ascending order
workingData = workingData[order(workingData$kDist),]
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(workingData$kDist,log="y",pch=20,cex=0.5)
#plot(workingData$kDist,log="y",xlab="index",ylab="kdistance",pch=20,cex=0.5)
title(main="kDist (logscale)",sub=titlePart,)
if(pngPlot)pngEnd()

densityVariations = ComputeDensityVariationsTER(workingData$kDist)
workingData$denvar = densityVariations
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(workingData$denvar,log="y",pch=20,cex=0.5)
plot(workingData$denvar,type="l",log="y",pch=20,cex=0.5)
plot(workingData$denvar,type="l",pch=20,cex=0.5)
#plot(workingData$denvar,log="y",xlab="index",ylab="density variation",pch=20,cex=0.5)
title(main="denvar (logscale)",sub=titlePart)
if(pngPlot)pngEnd()

rolldenvar = rollmean(workingData$denvar, windowSize)
rolldenvar = c(rolldenvar,rep(0,windowSize-1))
workingData$rolldenvar = rolldenvar
thrd_rollmean = mean(workingData$rolldenvar) + omega* sqrt(var(workingData$rolldenvar))


if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
#plot(workingData$rolldenvar,type="l", log="y",xlab="index",ylab="smoothed density variation",pch=20,cex=0.5)
plot(workingData$rolldenvar,type="l", log="y",pch=20,cex=0.5)
plot(workingData$rolldenvar,type="l",pch=20,cex=0.5)
#plot(workingData$rolldenvar,col=workingData$dls_RollMean,log="y",xlab="",ylab="",pch=20,cex=0.5)
#plot(workingData$rolldenvar,col=workingData$dls_RollMean,log="y",xlab="index",ylab="smoothed density variation",pch=20,cex=0.5)
abline(h=thrd_rollmean)
#abline(h=0.0000065)

title(main="rollDenvar (logscale)",sub=paste(titlePart,"; thrd:",thrd_rollmean))
if(pngPlot)pngEnd()
####move DOWN

#plot(rollmax(denvar$denvar,1000),log="y") ##logscale
densityLevelSetsRollMean = ComputeDensityLevelSets(workingData$rolldenvar, thrd_rollmean)
workingData$dls_RollMean = densityLevelSetsRollMean

#peaks=findpeaks(-workingData$rolldenvar, minpeakdistance = 30000)
#eps=workingData$kDist[sort(peaks[,2])]

mapTitle=paste("dataset Colored by dls",",",titlePart,"; thrd:",format(thrd_rollmean, digits=2, nsmall=2),"; #dls:",max(workingData$dls_RollMean))
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot_map(crimesData2012Features$longitude[workingData$id_instance],crimesData2012Features$latitude[workingData$id_instance],c=workingData$dls_RollMean,png=FALSE,file="myoutput.png", title=mapTitle,legend=TRUE,itemLabel="dls",pointsSize = mapPointsSize)
#plot_map(crimesData2012Features$longitude[workingData$id_instance],crimesData2012Features$latitude[workingData$id_instance],c=workingData$dls_RollMean,png=FALSE,file="myoutput.png", title="",legend=FALSE,itemLabel="dls",pointsSize = 0.5)
if(pngPlot)pngEnd()

if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(workingData$rolldenvar,col=workingData$dls_RollMean,log="y",pch=20,cex=0.5)
abline(h=thrd_rollmean)
title(main="rollDenvar (logscale), colored by dls",sub=paste(titlePart,"; thrd:",thrd_rollmean,"; #dls:",max(workingData$dls_RollMean)))
if(pngPlot)pngEnd()
#abline(h=thrd_rollmean2)
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(workingData$kDist,col=workingData$dls_RollMean,log="y",pch=20,cex=0.5)
title(main="kDist (logscale), colored by dls",sub=paste(titlePart,"; thrd:",thrd_rollmean,"; #dls:",max(workingData$dls_RollMean)))
if(pngPlot)pngEnd()

#epsValues_RollMean = ComputeEpsValues_RollMeanBIS(workingData)
epsValues_RollMean = ComputeEpsValues_RollMean(workingData)

if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(c(0, 1), c(0, 1), ann = F, bty = 'n', type = 'n', xaxt = 'n', yaxt = 'n')
epsValuesString=""
for (i in 1:length(epsValues_RollMean$epsValues_RollMean)) {
  epsValuesString=paste(epsValuesString,"\n","eps_",i,": ",epsValues_RollMean[i,2],sep="")
  
}
text(x = 0.5, y = 0.5, paste("eps Values Found.\n",
                             epsValuesString,"\n"
),cex = 1.6, col = "black")
if(pngPlot)pngEnd()
#abline(h=thrd_rollmean2)
if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(workingData$kDist,col=workingData$dls_RollMean,log="y",pch=20,cex=0.5)
plot(workingData$kDist,col=workingData$dls_RollMean,log="y",xlab="index",ylab="k distance",pch=20,cex=0.5)
title(main="kDist (logscale), colored by dls",sub=paste(titlePart,"; thrd:",thrd_rollmean,"; #dls:",max(workingData$dls_RollMean)))
for (i in 1:length(epsValues_RollMean$epsValues_RollMean)) {
  abline(h=epsValues_RollMean$epsValues_RollMean[i],col=i)
  
}
if(pngPlot)pngEnd()

#compute clustering by running several dbscan instances 
densityVariationMode = "roll_mean"
clusters = runDBScan_Instances(workingData, densityVariationMode, epsValues_RollMean, kvalue)
workingData$clusterID_RollMean = clusters

if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
mapTitle=paste("clusters",",",titlePart,"; thrd:",format(thrd_rollmean, digits=2, nsmall=2),"; #dls:",max(workingData$dls_RollMean),"; #clusters:",length(rownames(table(workingData$clusterID_RollMean))))
plot_map(crimesData2012Features$longitude[workingData$id_instance],crimesData2012Features$latitude[workingData$id_instance],c=workingData$clusterID_RollMean,png=FALSE,file="myoutput.png", title=mapTitle,legend=FALSE,pointsSize = mapPointsSize)
plot_map(crimesData2012Features$longitude[workingData$id_instance],crimesData2012Features$latitude[workingData$id_instance],c=workingData$clusterID_RollMean,png=FALSE,file="myoutput.png", title="",legend=FALSE,itemLabel="dls",pointsSize = 0.5,black0=TRUE)
if(pngPlot)pngEnd()

if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
barplot(100*(sort(table(workingData$clusterID_RollMean))/length(workingData$dls_RollMean)))
title(paste("clusters point%",",",titlePart,"; thrd:",format(thrd_rollmean, digits=2, nsmall=2),"; #dls:",max(workingData$dls_RollMean),"; #clusters:",length(rownames(table(workingData$clusterID_RollMean))))  )
if(pngPlot)pngEnd()

if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
barplot(computeClusterAvgDensity(kDists=workingData$kDist,kValue = kvalue, clusterIDs=workingData$clusterID_RollMean))
title(paste("clusters avg density",",",titlePart,"; thrd:",format(thrd_rollmean, digits=2, nsmall=2),"; #dls:",max(workingData$dls_RollMean),"; #clusters:",length(rownames(table(workingData$clusterID_RollMean))))  )
if(pngPlot)pngEnd()

if(pngPlot){pngNumber=pngNumber+1;pngStart(pngBaseName,pngNumber)}
plot(workingData$rolldenvar,col=workingData$clusterID_RollMean,log="y",pch=20,cex=0.5)
title(main="rollDenvar (logscale), colored by clusterID_rollMean",sub=paste(titlePart,"; thrd:",thrd_rollmean,"; #dls:",max(workingData$dls_RollMean),"; #clusters:",max(workingData$clusterID_RollMean)),itemLabel="cl")
if(pngPlot)pngEnd()
