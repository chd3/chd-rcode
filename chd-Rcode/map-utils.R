plot_map<-function(x,y,c=0,png=FALSE,file="myoutput.png", title="",legend=TRUE,itemLabel="cl",pointsSize=1,legendLabel="Clusters",black0=FALSE){
  
  library(ggplot2)
  library(ggmap)
  library(colorRamps)
  # if(max(c)!=0){
  #   #my_colors<-c("#000000","#ff0000","#00ffff","#0000ff","#00ff00","#ffff00","#ff00ff","#ff0080","#80ff00","#0080ff","#8000ff","#ff8000","#00ff80",matlab.like(max(c)))
  #   my_colors<-c("#000000","#ff0000","#0000ff","#00ffff","#ffff00","#00ff00","#ff00ff","#ff0080","#80ff00","#0080ff","#8000ff","#ff8000","#00ff80",matlab.like(max(c)))
  # }else{
  #   my_colors<-c("#000000")
  # }
  # if(length(c)!=0){
  #   clusterNumber=max(c);
  #   randomizedClusters=c
  #   generatedSample=sample(1:clusterNumber,clusterNumber,FALSE)+10000
  #   c[which(c==0)]=10000
  #   for(i in 1:clusterNumber){
  #     c[which(c==i)]=generatedSample[i];
  #   }
  #   c=c-10000
  # }
  
  register_google(key="jhjghfjgfhgfjhgjhghjfghjfgjhfghjghjfghj")#change it!
  lat_min<-min(y)
  lat_max<-max(y)
  lon_min<-min(x)
  lon_max<-max(x)
  lat_mean<-(lat_min+lat_max)/2
  lon_mean<-(lon_min+lon_max)/2
  
  #get the map
  
  
  myMap <- get_map(location = c(lon=lon_mean,lat=lat_mean), source = "google", zoom=12,scale=2)
  p <- ggmap(myMap)
  
  p
  
  
  #ClusterLevels<-c("Others", paste("Cluster ",1:max(c)))
  numCPoints=table(c)
  numC=1:length(rownames(numCPoints))
  numCstr=rep("",length(numC))

  for(i in 1:length(rownames(numCPoints))){
    numCstr[i]=paste(itemLabel,"_",rownames(numCPoints)[i]," (",numCPoints[paste("",rownames(numCPoints)[i],sep="")] ,"p)",sep="");
  }
  #ClusterLevels<-c(paste("Cl_",numC,sep=""))
  ClusterLevels<-numCstr
  ClusterLabels<-ClusterLevels[match(c,rownames(numCPoints))]

  Clusters<-factor(x = ClusterLabels,levels = ClusterLevels)

  my_data<-data.frame(x,y,Clusters)
  my_data<-my_data[order(c),] 
  
 
  p <- ggmap(myMap)
  p <- p+ggtitle(title)
  #p<-p + scale_color_manual(values = my_colors)
  
  #p<-p+geom_point(data=my_data, aes(x=x, y=y,color=Clusters),size=pointsSize,alpha=1/32)    
  #p<-p+geom_point(data=my_data, aes(x=x, y=y,color=Clusters),size=pointsSize)
  if(black0){
    my_data_0=my_data[my_data$Clusters==ClusterLevels[1],]
    my_data_others=my_data[my_data$Clusters!=ClusterLevels[1],]
    p<-p+geom_point(data=my_data_0, color="black",aes(x=my_data_0$x, y=my_data_0$y),size=pointsSize)    
    p<-p+geom_point(data=my_data_others, aes(x=my_data_others$x, y=my_data_others$y,color=Clusters),size=pointsSize)    
  }else{
    p<-p+geom_point(data=my_data, aes(x=x, y=y,color=Clusters),size=pointsSize)    
  }
  p<-p+ guides(colour = guide_legend(override.aes = list(alpha = 1)))
  if(legend){
    p<-p + theme(legend.position = "right",legend.title = element_blank())
  }else{
    p<-p + theme(legend.position = "none")
  }
  p<-p + theme(axis.title.x = element_blank())
  p<-p + theme(axis.title.y = element_blank())
  p<-p + theme(axis.text.x  = element_blank())
  p<-p + theme(axis.text.y  = element_blank())
  
  print(p)
  
  
  if(png==TRUE){
    
  png(filename =file,
      width = 200, height = 200, units = "mm", res=300)
  print(p)
  dev.off() 
  }
  
 
}


